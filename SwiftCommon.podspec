Pod::Spec.new do |spec|

spec.name         = "SwiftCommon"
spec.version      = "1.2.0"
spec.summary      = "Some of common using in Swift."
spec.description  = "Some of common using in Swift."

spec.platform     = :ios, "13.0"

spec.homepage     = "http://EXAMPLE/SwiftCommon"
spec.license      = "MIT"
spec.author             = { "Quang Tran" => "tranquangbk56@gmail.com" }

spec.source       = { :path => "." }

spec.subspec 'Extensions' do |extensions|
extensions.header_dir   =  'Extensions'
extensions.subspec 'XCFrameworkPod' do |xcframework|
  xcframework.vendored_frameworks = 'SwiftCommon.xcframework'
end
end

spec.subspec 'SocialSignIn' do |socialsignin|
socialsignin.header_dir   =  'SocialSignIn'
socialsignin.dependency "FacebookCore"
socialsignin.dependency "FacebookLogin"
socialsignin.dependency "GoogleSignIn"
socialsignin.subspec 'XCFrameworkPod' do |xcframework|
  xcframework.vendored_frameworks = 'SocialSignIn.xcframework'
end
end

end
